<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
 <!DOCTYPE>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		<h1>This is a JSP test!!!</h1>
		
		<div id="wrapper">
			<div id="header">
				<h3>Dummy Table Test</h3>
			</div>
		</div>
		
		<div id="container">
			<div id="content">
				<table>
					<tr>
						<th>ID</th>
						<th>DESCRIPTION</th>
					</tr>
					<c:forEach items="${dummies}" var="dummyItem">
						<tr>
							<td>${dummyItem.id}</td>
							<td>${dummyItem.description}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</body>
</html>
