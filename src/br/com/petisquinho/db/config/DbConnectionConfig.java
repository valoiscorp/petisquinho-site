package br.com.petisquinho.db.config;

import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;


@Configuration
@PropertySource("classpath:/resources/db.properties")
@EnableTransactionManagement
public class DbConnectionConfig {

	@Value("${DATASOURCE.MYSQL_DB_DRIVER_CLASS}")
	private String driverClass;

	@Value("${DATASOURCE.MYSQL_DB_URL}")
	private String url;
	
	@Value("${DATASOURCE.MYSQL_DB_USERNAME}")
	private String username;
	
	@Value("${DATASOURCE.MYSQL_DB_PASSWORD}")
	private String password;
		
	
	@Value("${HIBERNATE.C3P0.MIN_POOL_SIZE}")
	private int minPoolSize;
	
	@Value("${HIBERNATE.C3P0.MAX_POOL_SIZE}")
	private int maxPoolSize;
	
	@Value("${HIBERNATE.C3P0.ACQUIRE_INCREMENT}")
	private int acquireIncrement;
	
	@Value("${HIBERNATE.C3P0.MAX_IDLE_TIME}")
	private int maxIdleTime;
	
	@Value("${HIBERNATE.C3P0.MAX_STATEMENTS}")
	private int maxStatements;
	
	@Value("${HIBERNATE.C3P0.TIMEOUT}")
	private int timeout;
	

	@Value("${SESSIONFACTORY.PACKAGE_TO_SCAN}")
	private String packageToScan;
	
	@Value("${SESSIONFACTORY.DIALECT}")
	private String dialect;
	
	@Value("${SESSIONFACTORY.SHOW_SQL}")
	private String showSql;

	
	@Bean
	public DataSource getDataSource() {
		
		ComboPooledDataSource ds = new ComboPooledDataSource();
		try {
			ds.setDriverClass(driverClass);
			ds.setJdbcUrl(url);
			ds.setUser(username);
			ds.setPassword(password);
			
			ds.setMinPoolSize(minPoolSize);
			ds.setMaxPoolSize(maxPoolSize);
			ds.setAcquireIncrement(acquireIncrement);
			ds.setMaxIdleTime(maxIdleTime);
			ds.setMaxStatements(maxStatements);
			ds.setCheckoutTimeout(timeout);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		return ds;
	}

	@Bean
	public LocalSessionFactoryBean getSessionFactory() {
				
		LocalSessionFactoryBean fb = new LocalSessionFactoryBean();
		fb.setDataSource(getDataSource());
		fb.setPackagesToScan(packageToScan);
		
		Properties props = new Properties();
		props.put("hibernate.show_sql", showSql);
		props.put("hibernate.dialect", dialect);
		
		fb.setHibernateProperties(props);
		
		return fb;
	}
	
	@Bean
	public HibernateTransactionManager getTransactionManager() {
		
		HibernateTransactionManager tm = new HibernateTransactionManager();
		tm.setSessionFactory(getSessionFactory().getObject());
		return tm;
	}

}
