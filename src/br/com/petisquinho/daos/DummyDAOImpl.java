package br.com.petisquinho.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.petisquinho.db.config.DbConnectionConfig;
import br.com.petisquinho.entities.DummyEntity;


@Repository
public class DummyDAOImpl implements DummyDAO {
	
	@Autowired
	private DbConnectionConfig dbConnectionConfig;
	
	@Override
	@Transactional
	public List<DummyEntity> getDummies() {
				
		// get the current Hibernate session
		LocalSessionFactoryBean sf = dbConnectionConfig.getSessionFactory();
		Session currentSession = sf.getObject().getCurrentSession();
		
		// create a query
		Query<DummyEntity> theQuery = currentSession.createQuery("FROM DummyEntity", DummyEntity.class);
		
		// execute query and get result list
		List<DummyEntity> dummies = theQuery.getResultList();
		
		// return the results
		return dummies;
	}

}
