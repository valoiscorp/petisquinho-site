package br.com.petisquinho.daos;

import java.util.List;

import javax.persistence.Entity;

import br.com.petisquinho.entities.DummyEntity;

public interface DummyDAO {

	public List<DummyEntity> getDummies();
	
}
