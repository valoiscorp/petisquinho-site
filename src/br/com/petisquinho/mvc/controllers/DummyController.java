package br.com.petisquinho.mvc.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import br.com.petisquinho.daos.DummyDAO;
import br.com.petisquinho.entities.DummyEntity;


@Controller
@RequestMapping("/test")
public class DummyController {
	
	@Autowired
	private DummyDAO dummyDAO;

	
	@RequestMapping("/dummy")
	public String listDummies(Model theModel) {
		
		// get dummy from DAO
		List<DummyEntity> theDummies = dummyDAO.getDummies();
		
		theDummies.forEach(item->System.out.println(item));
		
		// add dummies to the model
		theModel.addAttribute("dummies", theDummies);

		return "test_dummy";
	}

}
