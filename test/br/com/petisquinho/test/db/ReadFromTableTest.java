package br.com.petisquinho.test.db;

import java.util.ArrayList;
import java.util.Arrays;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.petisquinho.db.config.DbConnectionConfig;
import br.com.petisquinho.entities.DummyEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {DbConnectionConfig.class})
@Transactional
public class ReadFromTableTest {

	@Autowired
	private DbConnectionConfig dbConfig;
	
	private ArrayList<DummyEntity> mockObject = new ArrayList<>();
	
	@Test
	public void testTableConnection() {
		
		mockObject.add(new DummyEntity(1, "Pacman"));
		mockObject.add(new DummyEntity(2, "Enduro"));
		mockObject.add(new DummyEntity(3, "River Raid"));	
		
		
		try {
			System.out.println("/*--------------------Testing Read From Table---------------------*/");
			
			// read the content of session factory
			LocalSessionFactoryBean sf = dbConfig.getSessionFactory();
			Session currentSession = sf.getObject().getCurrentSession();
			
			// create a query
			Query<DummyEntity> theQuery = currentSession.createQuery("FROM DummyEntity", DummyEntity.class);
			ArrayList<DummyEntity> dummies = (ArrayList<DummyEntity>) theQuery.getResultList();
			
			dummies.forEach(item->System.out.println(item));
			mockObject.forEach(item->System.out.println(item));
			
			Assert.assertEquals(true, arrayListComparison(mockObject, dummies));
			
		}catch(Exception exc) {
			//exc.printStackTrace();
			System.out.println(exc.getMessage());
		}
		System.out.println("/*----------------------------------------------------------------*/");
	}
	

	public boolean arrayListComparison(ArrayList<DummyEntity> expected, ArrayList<DummyEntity> actual) {
		
		for(DummyEntity a: expected) {
			for(DummyEntity b: actual) {
				if((a.getId() == b.getId()) && (!a.getDescription().equals(b.getDescription()))) {
					return false;
				}
			}
		}
		
		return true;
	}


}
