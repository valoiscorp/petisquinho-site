package br.com.petisquinho.test.db;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.petisquinho.db.config.DbConnectionConfig;	


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DbConnectionConfig.class)
public class DbConnectionTest {
	
	@Autowired
	private DataSource myDataSource;
	
	@Test
	public void testDBConnection() {
		Connection con;
		
		try {
			System.out.println("/*---------------------Testing DB Connection----------------------*/");
			System.out.println("Connecting to DB");
			con = myDataSource.getConnection();
			System.out.println("CONNECTION SUCCESSFULLY STABLISHED!");
			con.close();
			System.out.println("Connection closed");			
		}catch(SQLException exc) {
			//exc.printStackTrace();
			System.out.println(exc.getMessage());
		}
		System.out.println("/*----------------------------------------------------------------*/");
	}
	
}